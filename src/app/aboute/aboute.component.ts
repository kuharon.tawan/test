import { Component, OnInit } from '@angular/core';
import { Skill } from '../skill';
import { SkillService } from '../skill-service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-aboute',
  templateUrl: './aboute.component.html',
  styleUrls: ['./aboute.component.css']
})
export class AbouteComponent implements OnInit {
  skills: Observable<Skill>;

  constructor(private skillService: SkillService) {}

  ngOnInit() {
    this.reloadSkills();
  }

  reloadSkills() {
    this.skillService.reloadSkill().subscribe(result => {
      console.log(result);
      this.skills = result;
    });
  }
}
