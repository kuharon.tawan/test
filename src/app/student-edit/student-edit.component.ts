import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.css']
})
export class StudentEditComponent implements OnInit {
  studentsDetail = null;
  skills: any[] = [];
  skillSelect: any = null;
  selected: any[] = [];
  rating = null;

  constructor(private http: HttpClient, private route: ActivatedRoute) {}

  ngOnInit() {
    this.getStudentDetails();
    this.getSkill();
  }

  getStudentDetails() {
    this.http
      .get<any>(
        'http://localhost:8085/api/v1/students/' +
          this.route.snapshot.paramMap.get('id')
      )
      .subscribe(result => {
        this.studentsDetail = result.data;
        console.log(this.studentsDetail);
      });
  }

  getSkill() {
    this.http
      .get<any>('http://localhost:8085/api/v1/skills')
      .subscribe(result => {
        this.skills = result;
        console.log(this.skills);
      });
  }
  addSkill() {
    if (this.skillSelect) {
      const bodyData = {
        student_id: this.route.snapshot.paramMap.get('id'),
        skill_id: this.skillSelect,
        rating: this.rating
      };
      this.http
        .post<any>('http://localhost:8085/api/v1/skill', bodyData)
        .subscribe(result => {
          console.log(result);
          this.getStudentDetails();
        });
    }
  }

  selectSkill() {
    this.rating = 1;
    const skill: any[] = this.studentsDetail.studentSkills;
    skill.forEach(element => {
      console.log(element.skill_id + '', this.skillSelect);
      if (element.skill_id + '' === this.skillSelect) {
        this.rating = element.rating;
      }
    });
  }
}
