import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
  doing = true;
  students: any[] = [];

  constructor(private http: HttpClient) {}

  onSubmit(data) {
    this.http
      .post<any>('http://localhost:8085/api/v1/student', data)
      .subscribe(result => {
        this.getStudent();
        this.doing = true;
      });
  }

  getStudent() {
    this.http
      .get<any>('http://localhost:8085/api/v1/students')
      .subscribe(result => {
        console.log('testtt', result);
        this.students = result.data;
      });
  }
  deleteStudent(id) {
    this.http
      .delete<any>('http://localhost:8085/api/v1/student/' + id)
      .subscribe(result => {
        this.getStudent();
      });
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  ngOnInit() {
    this.getStudent();
  }
}
