import { HttpClient } from '@angular/common/http';

export class Student {
  students = [];

  constructor(private http: HttpClient) {
    this.http
      .get<any>('http://localhost:8085/api/v1/students')
      .subscribe(result => {
        this.students = result.data;
      });
  }

  updateStudent() {
    this.http
      .get<any>('http://localhost:8085/api/v1/students')
      .subscribe(result => {
        this.students = result.data;
      });
  }

  getStudent() {
    return this.students;
  }
}
