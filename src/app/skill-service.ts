import { Skill } from './skill';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SkillService {
  constructor(private http: HttpClient) {}

  reloadSkill(): Observable<any> {
    // console.log(environment.url);
    return this.http.get('http://localhost:8085/api/v1/skills');
  }
}
